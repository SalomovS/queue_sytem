package uz.pdp.govqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueSystemForPortfolioApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueueSystemForPortfolioApplication.class, args);
    }

}
