package uz.pdp.govqueue.service;

import uz.pdp.govqueue.payload.ApiResult;
import uz.pdp.govqueue.payload.GovServiceDTO;
import uz.pdp.govqueue.payload.LevelDTO;

import java.util.List;

public interface ServiceService {

    ApiResult<List<LevelDTO>> getServicesForQueue();

    GovServiceDTO create(GovServiceDTO govServiceDTO);

    ApiResult<List<GovServiceDTO>> getAllService(String orderByColumn,  int size, int page);

    void delete(Integer id);
}
