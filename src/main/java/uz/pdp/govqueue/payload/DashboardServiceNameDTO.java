package uz.pdp.govqueue.payload;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DashboardServiceNameDTO {

    private String id;

    private String name;
}
