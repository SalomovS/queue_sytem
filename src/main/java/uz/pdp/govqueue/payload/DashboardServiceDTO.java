package uz.pdp.govqueue.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DashboardServiceDTO {

    private List<DashboardServiceDateDTO> dates;

    private List<DashboardServiceNameDTO> services;
}
