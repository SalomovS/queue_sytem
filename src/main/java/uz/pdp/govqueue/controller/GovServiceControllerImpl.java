package uz.pdp.govqueue.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.govqueue.payload.ApiResult;
import uz.pdp.govqueue.payload.GovServiceDTO;
import uz.pdp.govqueue.payload.LevelDTO;
import uz.pdp.govqueue.service.ServiceService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GovServiceControllerImpl implements GovServiceController {

    private final ServiceService serviceService;

    @Override
    public HttpEntity<ApiResult<List<LevelDTO>>> forQueue() {
        ApiResult<List<LevelDTO>> servicesForQueue = serviceService.getServicesForQueue();
        return ResponseEntity.ok(servicesForQueue);
    }

    @Override
    public HttpEntity<GovServiceDTO> add(GovServiceDTO govServiceDTO) {
        govServiceDTO = serviceService.create(govServiceDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(govServiceDTO);
    }

    @Override
    public HttpEntity<ApiResult<List<GovServiceDTO>>> list(String orderByColumn, String search, int size, int page) {

        ApiResult<List<GovServiceDTO>> allService = serviceService.getAllService(orderByColumn,size,page);
        return ResponseEntity.ok(allService);
    }


    @Override
    public HttpEntity<?> delete(Integer id) {
        serviceService.delete(id);
        return (HttpEntity<?>) ResponseEntity.ok();
    }
}
