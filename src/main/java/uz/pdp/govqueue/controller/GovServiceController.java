package uz.pdp.govqueue.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.govqueue.payload.ApiResult;
import uz.pdp.govqueue.payload.GovServiceDTO;
import uz.pdp.govqueue.payload.LevelDTO;
import uz.pdp.govqueue.utils.AppConstants;

import java.util.List;

@RequestMapping(GovServiceController.BASE_PATH)
public interface GovServiceController {

    String BASE_PATH = AppConstants.BASE_PATH + "/service";

    String DELETE = "/{id}";
    @GetMapping("/for-queue")
    HttpEntity<ApiResult<List<LevelDTO>>> forQueue();

    @PostMapping
    HttpEntity<GovServiceDTO> add(@Valid @RequestBody GovServiceDTO govServiceDTO);

    //TODO pagination, sort, search
    @GetMapping
    HttpEntity<ApiResult<List<GovServiceDTO>>> list(
            @RequestParam(name = "orderByColumn", required = false) String orderByColumn,
            @RequestParam(name = "search", required = false) String search,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "page", required = false, defaultValue = "1") int page


    );

    @DeleteMapping(DELETE)
    HttpEntity<?> delete(@PathVariable Integer id);


}
